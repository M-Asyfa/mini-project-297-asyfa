package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.models.M_User;

public interface UserRegistrationRepository extends JpaRepository<M_User, Long> {
	
	@Query(value = "SELECT * FROM m_user WHERE email = :myEmail", nativeQuery = true)
	List<M_User> checkEmail(String myEmail);

}
