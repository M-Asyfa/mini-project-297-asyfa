package id.bootcamp.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.models.M_Customer_Relation;

public interface CustomerRelationRepository extends JpaRepository<M_Customer_Relation, Long> {
	
	@Query(value = "SELECT * FROM m_customer_relation WHERE is_deleted = false", nativeQuery = true)
	List<M_Customer_Relation> getAllCustomerRelations();

	@Query(value = "SELECT * FROM m_customer_relation WHERE is_deleted = false and id = :cari", nativeQuery = true)
	List<M_Customer_Relation> getCustomerRelationById(Long cari);
	
	@Query(value = "SELECT * FROM m_customer_relation WHERE name ilike %:cari% and is_deleted = false", nativeQuery = true)
	List<M_Customer_Relation> searchCustomerRelation(String cari);

//	Optional<M_Customer_Relation> findOne(String name);
}
