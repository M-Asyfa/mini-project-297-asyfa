package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.models.T_Token;

public interface TokenRegistrationRepository extends JpaRepository<T_Token, Long> {
	
	@Query(value = "SELECT * FROM t_token WHERE token = :myToken", nativeQuery = true)
	List<T_Token> checkToken(String myToken);
}
