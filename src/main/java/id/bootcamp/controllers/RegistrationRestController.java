package id.bootcamp.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.models.EmailModel;
import id.bootcamp.models.M_User;
import id.bootcamp.models.T_Token;
import id.bootcamp.services.RegistrationService;

@RestController
@RequestMapping("registrationApi")
public class RegistrationRestController {
	
	@Autowired
	private RegistrationService rs;
	
	@GetMapping("checkEmail")
	public String checkEmail(@RequestParam("email") String email) {
		List<M_User> checkedList = rs.checkEmail(email);
		
		if (checkedList.size() != 0) {
			return "Email sudah terdaftar";
		}
		else {
			return "Email belum terdaftar";
		}
	}

	@Autowired
	private JavaMailSender javaMailSender;
		
	@Value("${spring.mail.username}")
	private String sender;
	
	//Di sini ada 2 proses, yaitu
	//1. send otp ke email address
	//2. save otp ke tabel t_token
	@PostMapping("sendOTPCode")
	public String sendOTPCode(@RequestBody T_Token tm) {
		//1. Send OTP from server to user
		try {
			SimpleMailMessage smm = new SimpleMailMessage();
				
			//Set apa yang dibutuhkan untuk email
			smm.setFrom(sender);
			smm.setSubject("Kode OTP");
			smm.setTo(tm.getEmail());
			smm.setText(tm.getToken());
				
			//Kirim emailnya
			javaMailSender.send(smm);
				
		//2. Save OTP to t_token table
			tm.setCreated_by(1L);
			tm.setCreated_on(new Date());
			
			rs.saveOTP(tm);
			
			return "Kirim OTP Sukses";
		} catch (Exception e) {
			e.printStackTrace();
			return "Error Kirim OTP";
		}
	}
	
	@PostMapping("insertEmail")
	public String insertEmail(@RequestBody T_Token tm) {
		tm.setCreated_by(1L);;
		tm.setCreated_on(new Date());
		
		rs.insertEmail(tm);
		
		return "ok";
	}
}