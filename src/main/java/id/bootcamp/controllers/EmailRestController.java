package id.bootcamp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.models.EmailModel;

@RestController
@RequestMapping("emailApi")
public class EmailRestController {
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Value("${spring.mail.username}")
	private String sender;
	
	@PostMapping("sendMail")
	public String sendMail(@RequestBody EmailModel em) {
		try {
			SimpleMailMessage smm = new SimpleMailMessage();
			
			//Set apa yang dibutuhkan untuk email
			smm.setFrom(sender);
			smm.setSubject(em.getSubject());
			smm.setTo(em.getRecipient());
			smm.setText(em.getOtp());
			
			//Kirim emailnya
			javaMailSender.send(smm);
			
			return "Kirim Email Sukses";
		} catch (Exception e) {
			e.printStackTrace();
			return "Error Kirim Email";
		}
		
		
	}
}
