package id.bootcamp.controllers;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.models.M_Customer_Relation;
import id.bootcamp.services.CustomerRelationService;

@RestController
@RequestMapping("customerRelationApi")
public class CustomerRelationRestController {

	@Autowired
	private CustomerRelationService crs;
	
	@GetMapping("getAllCustomerRelations")
	public List<M_Customer_Relation> getAllCustomerRelations(){
		List<M_Customer_Relation> customerRelations = crs.getAllCustomerRelations();
		
		//Coba Java Stream Sorting & Filtering
		List<M_Customer_Relation> sortedCustomerRelations = 
				customerRelations.stream()
//				.sorted((id1,id2) -> (int)(id1.getId() - id2.getId()))
//				.sorted(Comparator.comparingLong(M_Customer_Relation::getId))
				.sorted((id1,id2) -> id1.getId().compareTo(id2.getId()))
//				.sorted((name2,name1) -> name1.getName().compareTo(name2.getName()))
				.collect(Collectors.toList());
						
//		return customerRelations;
		return sortedCustomerRelations;
	}
	
	@GetMapping("getCustomerRelationById/{id}")
	public M_Customer_Relation getCustomerRelation(@PathVariable("id") Long id) {
		return crs.getCustomerRelationById(id);
	}
	
	@PostMapping("insertCustomerRelation")
	public String insertCustomerRelation(@RequestBody M_Customer_Relation crm) {
		
		crm.setCreated_by(1L);
		crm.setCreated_on(new Date());
		crs.insertCustomerRelation(crm);
		
		return "ok";
	}
	
	@PutMapping("editCustomerRelation")
	public String editCustomerRelation(@RequestBody M_Customer_Relation crm) {
		M_Customer_Relation crSebelumnya = crs.getCustomerRelationById(crm.getId());
		crm.setCreated_by(crSebelumnya.getCreated_by());
		crm.setCreated_on(crSebelumnya.getCreated_on());
		
		crm.setModified_by(1L);
		crm.setModified_on(new Date());
		crs.editCustomerRelation(crm);
		
		return "ok";
	}
	
	@DeleteMapping("deleteCustomerRelation/{id}")
	public String deleteCustomerRelation(@PathVariable("id") Long id) {
		M_Customer_Relation crSebelumnya = crs.getCustomerRelationById(id);
		
		crSebelumnya.setIs_deleted(true);
		
		crSebelumnya.setDeleted_by(1L);
		crSebelumnya.setDeleted_on(new Date());
		
		crs.deleteCustomerRelation(crSebelumnya);
		
		return "ok";
	}
	
	@GetMapping("searchCustomerRelation")
	public List<M_Customer_Relation> searchCustomerRelation(@RequestParam String name) {
		List<M_Customer_Relation> customerRelations = crs.searchCustomerRelation(name);
		
		return customerRelations;
	}
}
