package id.bootcamp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomerRelationController {
	
	@RequestMapping("customerRelation")
	public String showCustomerRelationView() {
		return "customerRelation.html";
	}
}
