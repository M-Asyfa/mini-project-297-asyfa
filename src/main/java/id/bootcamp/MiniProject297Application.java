package id.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProject297Application {

	public static void main(String[] args) {
		SpringApplication.run(MiniProject297Application.class, args);
	}

}
