package id.bootcamp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bootcamp.models.EmailModel;
import id.bootcamp.models.M_User;
import id.bootcamp.models.T_Token;
import id.bootcamp.repositories.TokenRegistrationRepository;
import id.bootcamp.repositories.UserRegistrationRepository;

@Service
@Transactional
public class RegistrationService {

	@Autowired
	private UserRegistrationRepository urr;
	
	@Autowired
	private TokenRegistrationRepository trr;

	public RegistrationService(UserRegistrationRepository urr, TokenRegistrationRepository trr) {
		super();
		this.urr = urr;
		this.trr = trr;
	}
	
	public List<M_User> checkEmail(String myEmail) {
		return urr.checkEmail(myEmail);
	}
	
	//insertEmail
	public void insertEmail(T_Token tm) {
			
		trr.save(tm);
	}
	
	//sendOTPCode
//	public void sendOTPCode(T_Token tm) {
//		
//		trr.save(tm);
//	}
	
	//insertOTPCode
	public void saveOTP(T_Token tm) {
	
		trr.save(tm);
	}
	
	//
	
	
	
}
