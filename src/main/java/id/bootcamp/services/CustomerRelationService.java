package id.bootcamp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.bootcamp.models.M_Customer_Relation;
import id.bootcamp.repositories.CustomerRelationRepository;

@Service
@Transactional
public class CustomerRelationService {
	
	@Autowired
	private CustomerRelationRepository crr;
	
	public CustomerRelationService(CustomerRelationRepository crr) {
		super();
		this.crr = crr;
	}

	public List<M_Customer_Relation> getAllCustomerRelations() {
		return crr.getAllCustomerRelations();
	}
	
	//getCustomerMemberById
	public M_Customer_Relation getCustomerRelationById(Long id) {
		return crr.findById(id).get();
	}
	
	//insertCustomerMember
	public void insertCustomerRelation(M_Customer_Relation crm) {
		crr.save(crm);
	}
	
	//editCustomerMember
	public void editCustomerRelation(M_Customer_Relation crm) {
		crr.save(crm);
	}
	
	//deleteCustomerMember
	public void deleteCustomerRelation(M_Customer_Relation crm) {
		crr.save(crm);
	}
	
	public List <M_Customer_Relation> searchCustomerRelation(String name) {
		return crr.searchCustomerRelation(name);
	}
}
